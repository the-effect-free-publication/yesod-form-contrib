{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TypeFamilies #-}

module Yesod.Form.Bootstrap5 (BS5GridOptions (..), BS5FormLayout (..), renderBootstrap5) where

import Data.Text (Text)
import Text.Blaze.Internal qualified as Blaze
import Yesod.Core (WidgetFor, whamlet)
import Yesod.Form.Functions (FormRender, aFormToForm)
import Yesod.Form.Types (
    FieldView (fvErrors, fvId, fvInput, fvLabel, fvTooltip),
 )

data BS5GridOptions
    = ColXS !Int
    | ColSM !Int
    | ColMD !Int
    | ColLG !Int
    | ColXL !Int
    | ColXXL !Int
    deriving (Eq, Ord, Show)

toColumn :: BS5GridOptions -> String
toColumn (ColXS 0) = ""
toColumn (ColSM 0) = ""
toColumn (ColMD 0) = ""
toColumn (ColLG 0) = ""
toColumn (ColXL 0) = ""
toColumn (ColXXL 0) = ""
toColumn (ColXS column) = "col-" ++ show column
toColumn (ColSM column) = "col-sm-" ++ show column
toColumn (ColMD column) = "col-md-" ++ show column
toColumn (ColLG column) = "col-lg-" ++ show column
toColumn (ColXL column) = "col-xl-" ++ show column
toColumn (ColXXL column) = "col-xxl-" ++ show column

toOffset :: BS5GridOptions -> String
toOffset (ColXS 0) = ""
toOffset (ColSM 0) = ""
toOffset (ColMD 0) = ""
toOffset (ColLG 0) = ""
toOffset (ColXL 0) = ""
toOffset (ColXXL 0) = ""
toOffset (ColXS columns) = "offset-" ++ show columns
toOffset (ColSM columns) = "offset-sm-" ++ show columns
toOffset (ColMD columns) = "offset-md-" ++ show columns
toOffset (ColLG columns) = "offset-lg-" ++ show columns
toOffset (ColXL columns) = "offset-xl-" ++ show columns
toOffset (ColXXL columns) = "offset-xxl-" ++ show columns

addGO :: BS5GridOptions -> BS5GridOptions -> BS5GridOptions
addGO (ColXS a) (ColXS b) = ColXS (a + b)
addGO (ColSM a) (ColSM b) = ColSM (a + b)
addGO (ColMD a) (ColMD b) = ColMD (a + b)
addGO (ColLG a) (ColLG b) = ColLG (a + b)
addGO (ColXL a) (ColXL b) = ColXL (a + b)
addGO (ColXXL a) (ColXXL b) = ColXXL (a + b)
addGO a b | a > b = addGO b a
addGO (ColXS a) other = addGO (ColSM a) other
addGO (ColSM a) other = addGO (ColMD a) other
addGO (ColMD a) other = addGO (ColLG a) other
addGO (ColLG a) other = addGO (ColXL a) other
addGO _ _ = error "Yesod.Form.Bootstrap.addGO: never here"

-- | The layout used for the bootstrap form.
data BS5FormLayout
    = BS5BasicForm
    | BS5IntlineForm
    | BS5HorizontalForm
        { bs5flLabelOffset :: !BS5GridOptions
        , bs5flLabelSize :: !BS5GridOptions
        , bs5flInputOffset :: !BS5GridOptions
        , bs5flInputSize :: !BS5GridOptions
        }
    deriving (Show)

-- | (Internal) Render a help widget for tooltips and errors.
helpWidget :: FieldView site -> WidgetFor site ()
helpWidget view =
    [whamlet|
    $maybe tt <- fvTooltip view
        <div .form-text>#{tt}
    $maybe err <- fvErrors view
        <div .invalid-feedback>#{err}
    |]

{- | A royal hack.  Magic id used to identify whether a field
 should have no label.  A valid HTML4 id which is probably not
 going to clash with any other id should someone use
 'bootstrapSubmit' outside 'renderBootstrap5'.
-}
bootstrapSubmitId :: Text
bootstrapSubmitId = "b:ootstrap___unique__:::::::::::::::::submit-id"

-- | Render the given form using Bootstrap 5 conventions.
renderBootstrap5 :: Monad m => BS5FormLayout -> FormRender m a
renderBootstrap5 formLayout aform fragment = do
    (res, views') <- aFormToForm aform
    let views = views' []
        has (Just _) = True
        has Nothing = False
        widget =
            [whamlet|
                $newline never
                #{fragment}
                $forall view <- views
                    $case formLayout
                        $of BS5BasicForm
                            <div .mb-3>
                                $if fvId view /= bootstrapSubmitId
                                    <label :Blaze.null (fvLabel view):.sr-only .form-label for=#{fvId view}>#{fvLabel view}
                                ^{fvInput view}
                                ^{helpWidget view}
                        $of BS5IntlineForm
                            <div .input-group .mb-3>
                                $if fvId view /= bootstrapSubmitId
                                    <label .sr-only .form-label for=#{fvId view}>#{fvLabel view}
                                ^{fvInput view}
                                ^{helpWidget view}
                        $of BS5HorizontalForm labelOffset labelSize inputOffset inputSize
                            <div .mb-3 .row> 
                                $if fvId view /= bootstrapSubmitId
                                    <label :Blaze.null (fvLabel view):.sr-only .form-label .#{toOffset labelOffset} .#{toColumn labelSize} for=#{fvId view}>#{fvLabel view}
                                    <div .#{toOffset inputOffset} .#{toColumn inputSize}>
                                        ^{fvInput view}
                                        ^{helpWidget view}
                                $else
                                    <div .#{toOffset (addGO inputOffset (addGO labelOffset labelSize))} .#{toColumn inputSize}>
                                        ^{fvInput view}
                                        ^{helpWidget view}
            |]
    return (res, widget)